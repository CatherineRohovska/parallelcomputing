//
//  computions.metal
//  ParallelComputing
//
//  Created by User on 10/9/15.
//  Copyright © 2015 User. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;


kernel void
add_vectors(const device float *inA [[ buffer(0) ]],
          /* const device float *inB [[ buffer(1) ]],*/
            device float *out [[ buffer(1) ]],
            uint id [[ thread_position_in_grid ]])
{
    // This calculates for _one_ position (=id) in a vector per call on the GPU
    float tmp = (float) 1/inA[id];//*inA[id];
    out[id] = tmp;//+(float)inA[id];//id;//inA[id] + inB[id];
}

kernel void anaglyph(
                        texture2d<float,access::read>   inputImageLeft   [[ texture(0) ]],
                        texture2d<float,access::read>   inputImageRight   [[ texture(1) ]],
                        texture2d<float,access::write>  outputImage  [[ texture(2) ]],
                        uint2 gid                                    [[ thread_position_in_grid ]]
                        )
{
    //float2 p0          = static_cast<float2>(gid);
    float4 pixel, pixelLeft, pixelRight;
    
    ///
    pixelLeft.r = inputImageLeft.read(gid).r;
    pixelLeft.g = 0;
    pixelLeft.b = 0;
    pixelLeft.a = 0.5;
    ///
    pixelRight.r = 0;
    pixelRight.g = inputImageRight.read(gid).g;
    pixelRight.b = inputImageRight.read(gid).b;
    pixelRight.a = 0.5;
    //
    pixel = pixelLeft+pixelRight;
    outputImage.write(pixel,gid,0);
}