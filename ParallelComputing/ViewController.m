//
//  ViewController.m
//  ParallelComputing
//
//  Created by User on 10/9/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ViewController.h"
@import MetalKit;
#import <Metal/Metal.h>
#import <GLKit/GLKit.h>
#import "CalculationEngine.h"
@interface ViewController (){
    float from;
    float to;
    int count;
    float* resultv;
    float* vector1;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    count = 64;
    from =1;
    to=2;
    vector1 = malloc(sizeof(float)*count);
    float* vector2 = malloc(sizeof(float)*count);
    resultv = malloc(sizeof(float)*count);
    //
    for (int i=0; i<count; i++) {
        vector1[i]=(float)i+0.0f;
        vector2[i]=(float)i+10+0.0f;
        resultv[i]=(float)111+0.0f;
    }
[_sliderFrom addTarget:self action:@selector(fromValueChanged) forControlEvents:UIControlEventValueChanged];
[_sliderTo addTarget:self action:@selector(toValueChanged) forControlEvents:UIControlEventValueChanged];
}
- (void) fromValueChanged
{  _sliderFrom.value = round(_sliderFrom.value);
    from = _sliderFrom.value;
    _sliderTo.minimumValue = _sliderFrom.value+1;
    _sliderTo.value = _sliderTo.minimumValue;
    _sliderTo.maximumValue = _sliderTo.minimumValue+1000;
     _labelFrom.text = [NSString stringWithFormat:@"%.0f", from];
    to = _sliderTo.value;
    _labelTo.text = [NSString stringWithFormat:@"%.0f", to];
}
-(void) toValueChanged
{
    _sliderTo.value = round(_sliderTo.value);
    to = _sliderTo.value;
    _labelTo.text = [NSString stringWithFormat:@"%.0f", to];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClick:(id)sender {

    count = to - from;
    CalculationEngine* calc = [CalculationEngine new];

    [calc defineIntegral:from to:to];
    resultv = calc.outputArray;
    float k = [calc getIntegral];
    _labelResult.text = [NSString stringWithFormat:@"%.5f", k];
     NSLog(@"%f", resultv[0]);
    
}
@end
