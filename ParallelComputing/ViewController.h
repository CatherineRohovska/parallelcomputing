//
//  ViewController.h
//  ParallelComputing
//
//  Created by User on 10/9/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *buttonCheck;
- (IBAction)buttonClick:(id)sender;
@property (weak, nonatomic) IBOutlet UISlider *sliderFrom;
@property (weak, nonatomic) IBOutlet UISlider *sliderTo;
@property (weak, nonatomic) IBOutlet UILabel *labelFrom;
@property (weak, nonatomic) IBOutlet UILabel *labelTo;
@property (weak, nonatomic) IBOutlet UILabel *labelResult;

@end

