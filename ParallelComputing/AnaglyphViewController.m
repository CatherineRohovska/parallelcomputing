//
//  AnaglyphViewController.m
//  ParallelComputing
//
//  Created by User on 10/12/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "AnaglyphViewController.h"
#import "CalculationEngine.h"
@implementation AnaglyphViewController
-(void) viewDidLoad
{
    [super viewDidLoad];
    float side= 0;
    
    if (self.view.bounds.size.width>self.view.bounds.size.height) {
        side = self.view.bounds.size.height;
        viewFrame = CGRectMake(side/3, 0, side, side);
    }
    else{
        side = self.view.bounds.size.width;
         viewFrame = CGRectMake(0, side/3, side, side);
    }
    imgView = [UIImageView new];
    [imgView setFrame:viewFrame];
   // imgView.contentMode =
   
    imgView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin);
    ///
    //cutomizing button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"New" forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"button_background.png"] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btn setFrame:CGRectMake(0.0f, 0.0f, 80.0f, 30.0f)];
    [btn addTarget:self action:@selector(convertPhoto) forControlEvents:UIControlEventTouchUpInside];
    //adding button to Navigator
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
     self.navigationItem.rightBarButtonItem = anotherButton;
 // _imageView.frame = CGRectMake(0, 0, side, side);
    //load textures from image
 //   UIImage *image = [UIImage imageNamed:@"cubic.jpg"];

  //  CalculationEngine *stereoMaker = [CalculationEngine new];
   //  _imageView.image = [stereoMaker createAnaglyph:image];
//    UIImage* imageAnaglyph =[stereoMaker createAnaglyph:image];
//    imgView.image =imageAnaglyph;
//    viewFrame.size.width = imageAnaglyph.size.width/(imageAnaglyph.size.height/viewFrame.size.height);
//     imgView.frame = viewFrame;
    [self.view addSubview:imgView];
    [self createMenu];
}
//
-(void) convertPhoto
{
    if (imgView.image) {
      
    UIImage *image = imgView.image;//[UIImage imageNamed:@"cubic.jpg"];
    
    CalculationEngine *stereoMaker = [CalculationEngine new];
    //  _imageView.image = [stereoMaker createAnaglyph:image];
    UIImage* imageAnaglyph =[stereoMaker createAnaglyph:image];
    imgView.image =imageAnaglyph;
//    viewFrame.size.width = imageAnaglyph.size.width/(imageAnaglyph.size.height/viewFrame.size.height);
   // imgView.frame = viewFrame;
    }
}
//
-(void) createMenu{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) //if camera exists
    {
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"
                                             destructiveButtonTitle:nil otherButtonTitles:
                                @"Library",
                                @"Camera",
                                nil];
        popup.tag = 1;
        [popup showInView:[UIApplication sharedApplication].keyWindow];
       
    }
    else{
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                @"Library",
                                nil,
                                nil];
        popup.tag = 1;
        [popup showInView:[UIApplication sharedApplication].keyWindow];
        
    }
}
//connect with action sheet
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if ([[popup buttonTitleAtIndex:buttonIndex] isEqual:@"Library"]) {
        [self clickLibrary];
    }
    if ([[popup buttonTitleAtIndex:buttonIndex] isEqual:@"Cancel"]) {
        [self clickCancel];
    }
    if ([[popup buttonTitleAtIndex:buttonIndex] isEqual:@"Camera"]) {
        [self clickCamera];
    }
}
//
- (void) clickCancel //cancel
{
 
    [self.navigationController setNavigationBarHidden:false];
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void) clickCamera //with camera
{

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
   
    
}
- (void) clickLibrary //without camera
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}
//
//reload methods from imagePickerController
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    imgView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    //    Screen3 *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"Screen3"];
    //    //ctrl.flag = YES;
    //    [self.navigationController pushViewController:ctrl animated:YES];
//    [self.navigationController setNavigationBarHidden:true];
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
}

-(void) dealloc
{
    NSLog(@"dealloc called");
}
@end
