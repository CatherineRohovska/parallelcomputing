//
//  CalculationEngine.m
//  ParallelComputing
//
//  Created by User on 10/12/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "CalculationEngine.h"

@implementation CalculationEngine
-(void) defineIntegral: (float) beginInterval to: (float) endInterval
{
    dataCount = (endInterval-beginInterval)*100;
    _inputArray = malloc(sizeof(float)*dataCount);
    _outputArray = malloc(sizeof(float)*dataCount);
    _inputArray[0]=beginInterval;
    for (int i=1; i<dataCount; i++) {
        _inputArray[i]=_inputArray[i-1]+0.01f;
    }
    device = MTLCreateSystemDefaultDevice();
    commandQueue = [device newCommandQueue];
    library = [device newDefaultLibrary];
    
    // Obtain a new command buffer
    id <MTLCommandBuffer> commandBuffer = [commandQueue commandBuffer];
    
    // Create a compute command encoder
    id <MTLComputeCommandEncoder> computeCE = [commandBuffer computeCommandEncoder];
    NSError *errors;
    id <MTLFunction> func = [library newFunctionWithName:@"add_vectors"];
    pipelineState = [device newComputePipelineStateWithFunction:func error:&errors];
    [computeCE setComputePipelineState:pipelineState];
    inputBuffer = [device newBufferWithBytes:_inputArray length:sizeof(float)*dataCount options:0];
    outputBuffer =  [device newBufferWithBytes:_inputArray length:sizeof(float)*dataCount options:0];
    [computeCE setBuffer:inputBuffer offset:0 atIndex:0];
    [computeCE setBuffer:outputBuffer offset:0 atIndex:1];
    int blocksize = 32;
    MTLSize threadsPerGroup = {blocksize,1,1}; //width:32,height:1,depth:1
    MTLSize numThreadgroups = {(dataCount+blocksize-1)/blocksize, 1, 1}; //
    
    [computeCE dispatchThreadgroups:numThreadgroups
              threadsPerThreadgroup:threadsPerGroup];
    [computeCE endEncoding];
    
    [commandBuffer commit];
    //wait until operation calculates
    [commandBuffer waitUntilCompleted];

    NSData *data = [NSData dataWithBytesNoCopy:outputBuffer.contents length:sizeof(float)*dataCount freeWhenDone:NO];
    data = [NSData dataWithBytes:outputBuffer.contents length:sizeof(float)*dataCount];
    //float* result = malloc(sizeof(float)*count);
    [data getBytes:_outputArray length:sizeof(float)*dataCount];
    
}
- (float) getIntegral
{
    float sum=0;
    for (int i=0; i<dataCount; i++) {
        sum=sum+_outputArray[i];
    }
    return sum/100;
}
//image processing
-(UIImage*) createAnaglyph: (UIImage*) source
{
    //future textures
    id <MTLTexture> inputImageLeft;
    id <MTLTexture> outputImage;
    id <MTLTexture> inputImageRight;
    //
    CGImageRef imageRef = [source CGImage];
    
    // Create a suitable bitmap context for extracting the bits of the image
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    uint8_t *rawData = (uint8_t *)calloc(height * width * 4, sizeof(uint8_t));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    //create left image
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    MTLTextureDescriptor* textureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatRGBA8Unorm
                                                                                                 width:width
                                                                                                height:height
                                                                                             mipmapped:NO];
    
    
    device = MTLCreateSystemDefaultDevice();
    //---create texture
    inputImageLeft = [device newTextureWithDescriptor:textureDescriptor];
    //create right image the same size
    inputImageRight = [device newTextureWithDescriptor:textureDescriptor];
    context = CGBitmapContextCreate(rawData, width, height,
                                    bitsPerComponent, bytesPerRow, colorSpace,
                                    kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    textureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatRGBA8Unorm                                                                                                 width:width                                                                                                height:height                                                                                             mipmapped:NO];
    
    // create device
    
    device = MTLCreateSystemDefaultDevice();
    //---create texture
    inputImageLeft = [device newTextureWithDescriptor:textureDescriptor];
    //
    outputImage = [device newTextureWithDescriptor:textureDescriptor];
    MTLRegion region = MTLRegionMake2D(0, 0, width, height);
    //replace context left;
    [inputImageLeft replaceRegion:region mipmapLevel:0 withBytes:rawData bytesPerRow:bytesPerRow];
    //create new region right - move picture by 40 px right
   MTLRegion regionReplacement = MTLRegionMake2D(40, 0, width-40, height);
     MTLRegion regionRight = MTLRegionMake2D(0, 0, 40, height);
    [inputImageRight replaceRegion:regionReplacement mipmapLevel:0 withBytes:rawData bytesPerRow:bytesPerRow];
    [inputImageRight replaceRegion:regionRight mipmapLevel:0 withBytes:rawData bytesPerRow:bytesPerRow];
    //--end creating
    commandQueue = [device newCommandQueue];
    library = [device newDefaultLibrary];
    
    // Obtain a new command buffer
    id <MTLCommandBuffer> commandBuffer = [commandQueue commandBuffer];
    
    // Create a compute command encoder
    id <MTLComputeCommandEncoder> computeCE = [commandBuffer computeCommandEncoder];
    NSError *errors;
    id <MTLFunction> func = [library newFunctionWithName:@"anaglyph"];
    pipelineState = [device newComputePipelineStateWithFunction:func error:&errors];
    [computeCE setComputePipelineState:pipelineState];
    //
    [computeCE setTexture:inputImageLeft atIndex:0];
    [computeCE setTexture:inputImageRight atIndex:1];
    [computeCE setTexture:outputImage atIndex:2];
    MTLSize threadsPerGroup = {16, 16, 1};
    MTLSize numThreadgroups = {inputImageLeft.width/threadsPerGroup.width,
        inputImageLeft.height/threadsPerGroup.height, 1};
    
    [computeCE dispatchThreadgroups:numThreadgroups
              threadsPerThreadgroup:threadsPerGroup];
    //copy data
    uint8_t *rawDataOutput = (uint8_t *)calloc(height * width * 4, sizeof(uint8_t));
    //
    [computeCE endEncoding];
    [commandBuffer commit];
    [commandBuffer waitUntilCompleted];
    //[inputImage getBytes:rawData2 bytesPerRow:bytesPerRow fromRegion:region mipmapLevel:0];
    
    [outputImage getBytes:rawDataOutput bytesPerRow:bytesPerRow fromRegion:regionReplacement mipmapLevel:0];
    CGContextRef contextOutput = CGBitmapContextCreate(rawDataOutput, width-40, height,
                                                  bitsPerComponent, bytesPerRow, colorSpace,
                                                  kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    //make image reference
    CGImageRef imageRefOutput = CGBitmapContextCreateImage(contextOutput);
    CGContextRelease(contextOutput);
    

    UIImage* img =[UIImage imageWithCGImage:imageRefOutput];
    return img;
}

@end
