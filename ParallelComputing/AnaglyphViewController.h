//
//  AnaglyphViewController.h
//  ParallelComputing
//
//  Created by User on 10/12/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Metal;
#import <MetalKit/MetalKit.h>
@interface AnaglyphViewController : UIViewController
<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>
{
    id <MTLDevice> device;
    id <MTLLibrary> library;
    id <MTLCommandQueue> commandQueue;
    id <MTLComputePipelineState> pipelineState;
    id <MTLTexture> inputImageLeft;
    id <MTLTexture> outputImage;
    id <MTLTexture> inputImageRight;
    //
    UIImageView* imgView;
    CGRect viewFrame;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
