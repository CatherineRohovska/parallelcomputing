//
//  CalculationEngine.h
//  ParallelComputing
//
//  Created by User on 10/12/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>
@import MetalKit;
@interface CalculationEngine : NSObject
{
    id <MTLDevice> device;
    id <MTLLibrary> library;
    id <MTLCommandQueue> commandQueue;
    id <MTLComputePipelineState> pipelineState;
    id<MTLBuffer> inputBuffer;
    id<MTLBuffer> outputBuffer;
    int dataCount;
    
}
@property (nonatomic) float* inputArray;
@property(nonatomic, readonly) float* outputArray;

-(void) defineIntegral: (float) beginInterval to: (float) endInterval;
- (float) getIntegral;
-(UIImage*) createAnaglyph: (UIImage*) source;
@end
